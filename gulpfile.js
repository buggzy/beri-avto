var gulp = require('gulp'),
    less = require('gulp-less'),
    ttf2woff = require('gulp-ttf2woff'),
    ttf2eot = require('gulp-ttf2eot'),
    autoprefixer = require('gulp-autoprefixer'),
    cssnano = require('gulp-cssnano'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    notify = require('gulp-notify'),
    cache = require('gulp-cache'),
    fileinclude = require('gulp-file-include'),
    livereload = require('gulp-livereload'),
    sourcemaps = require('gulp-sourcemaps'),
    base64 = require('gulp-base64'),
    wait = require('gulp-wait'),
    del = require('del');

const pngquant = require('imagemin-pngquant');

gulp.task('styles', ['images'], function() {
  return gulp.src(['src/vendor/css/*.css', 'src/styles/*.less'])
    .pipe(wait(1000))
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(autoprefixer('last 2 versions'))
    .pipe(base64({
            baseDir: 'dist/images',
            extensions: ['png', 'jpg'],
            maxImageSize: 8*1024, // bytes 
//            debug: true
        }))
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(rename({suffix:'-min'}))
    .pipe(gulp.dest('dist/css'))
});

gulp.task('scripts', function() {
  return gulp.src(['src/vendor/js/**/*.js', 'src/scripts/*.js'])
    .pipe(wait(1000))
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('dist/js'))
    .pipe(uglify())
    .pipe(rename({suffix:'-min'}))
    .pipe(gulp.dest('dist/js'))
});

gulp.task('html', function() {
    return gulp.src('src/html/**/*')
    .pipe(wait(1000))
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task('images', function() {
    return gulp.src('src/images/**/*')
	.pipe(wait(1000))
	.pipe(imagemin({
			progressive: true,
			svgoPlugins: [{removeViewBox: false}],
			use: [pngquant()]
		}))
	.pipe(gulp.dest('dist/images'));
});

gulp.task('fonts', function() {
    gulp.start('ttf2woff', 'ttf2eot', 'ttf2ttf');
});


gulp.task('ttf2woff', function(){
  gulp.src(['src/fonts/*.ttf'])
    .pipe(ttf2woff())
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('ttf2eot', function(){
  gulp.src(['src/fonts/*.ttf'])
    .pipe(ttf2eot())
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('ttf2ttf', function(){
  gulp.src(['src/fonts/*.ttf'])
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('default', function() {
    gulp.start('styles', 'scripts', 'html', 'images', 'watch');
});

gulp.task('watch', function() {


  // Watch styles files
  gulp.watch('src/styles/**/*' , ['styles']);

  // Watch html files
  gulp.watch('src/**/*.html' , ['html']);

  // Watch .js files
  gulp.watch('src/**/*.js', ['scripts']);

  // Watch image files
  gulp.watch('src/images/**/*' , ['styles']);

  // Watch image files
  gulp.watch('src/fonts/**/*', ['fonts']);

});
