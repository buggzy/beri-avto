$(function (){

	$('section.home-slider .owl-carousel').owlCarousel({
		items: 1,
		nav: true,
		dots: false,
		navText: ['',''],
		loop: true,
		autoplay: true
	});

	$('section.catalog-best .catalog-carousel.owl-carousel').owlCarousel({
		items: 5,
		nav: true,
		dots: false,
		navText: ['',''],
		loop: true,
		autoplay: true,
		margin: 50
	});

	$('.tabbed-catalog-carousel .nav-tabs a').click(function (ev){
		ev.preventDefault();

		$(this).closest('.nav-pill')
			.addClass('active').siblings().removeClass('active');

		var href = $(this).attr('href');
		$(href).addClass('active').siblings().removeClass('active');

		initTabCarousel();
	});

	initTabCarousel();

	function initTabCarousel(){
	    $('.tabbed-catalog-carousel .tab.active .catalog-carousel.owl-carousel:not(.owl-theme)').owlCarousel({
		items: 4,
		nav: true,
		dots: false,
		navText: ['',''],
		loop: true,
		autoplay: true,
		margin: 60
	    });
	}

	$('.fancybox').fancybox();

	$('[data-popup]').click(function (ev){
		ev.preventDefault();
		var target = $(this).data('popup');
		$(target).removeClass('hidden');
		$('.popup-bg').removeClass('hidden');
	});

	$('.closepopup').click(function (ev){
		ev.preventDefault();
		$('.popup-bg, .popup').addClass('hidden');
	});

});
