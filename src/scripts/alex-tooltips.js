$(function (){

    function validate(items) {
        items.removeClass('invalid');
        var retval = true;
        items.each(function(i, e) {
            if ($(e).val() == '') {
                $(e).addClass('invalid');
                retval = false;
            }
        });
        return retval;
    }

    $(".checkform").click(function(ev) {

        if (!validate($(this).closest('form').find('input.required, textarea.required'))){
	    ev.preventDefault();
            return false;
	}


        if ($(this).is('.showthanks')) {
            $('.modal.in').modal('hide');
            $('#modal-thanks').modal('show');
        }
    });


    $("body").bind({
        click: function() {
            $("input, textarea").removeClass("invalid");
        },
        keyup: function() {
            $("input, textarea").removeClass("invalid")
        }
    });

});
